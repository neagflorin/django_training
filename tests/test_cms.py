import pytest
from cmsapp.models import Page, Tag


@pytest.fixture
def authed_client(db, client, admin_user):
    client.force_login(admin_user)
    return client


@pytest.fixture
def client(request, client): # pragma: no cover
    func = client.request

    def wrapper(**kwargs):
        print('>>>>', ' '.join('{}={!r}'.format(*item)
                               for item in kwargs.items()))
        resp = func(**kwargs)
        print('<<<<', resp, resp.content)
        return resp

    client.request = wrapper
    return client


@pytest.fixture
def pages(request, db):
    pages = [
        Page.objects.create(slug='abc', title='ABC', content='lorem ipsum'),
        Page.objects.create(slug='123', title='1 2 3', content='lorem ipsum'),
        Page.objects.create(slug='foo', title='Bar', content='lorem ipsum')
    ]
    return pages


@pytest.fixture
def tags(request, db):
    tags = [
        Tag.objects.create(page=Page.objects.get(slug="abc"), name="a"),
        Tag.objects.create(page=Page.objects.get(slug="abc"), name="b"),
        Tag.objects.create(page=Page.objects.get(slug="123"), name="a"),
        Tag.objects.create(page=Page.objects.get(slug="123"), name="b"),
        Tag.objects.create(page=Page.objects.get(slug="123"), name="c"),
        Tag.objects.create(page=Page.objects.get(slug="foo"), name="b"),
        Tag.objects.create(page=Page.objects.get(slug="foo"), name="c")
    ]
    return tags


def test_index(pages, tags, client):
    resp = client.get('/cms/')
    assert resp.status_code == 200
    assert list(resp.context['pages']) == pages
    content = resp.content.decode(resp.charset)
    for page in pages:
        assert page.title in content
    resp = client.get('/cms/:a/')
    assert resp.status_code == 200
    assert list(resp.context['pages']) == pages[:2]
    assert [item.values()[0] for item in list(resp.context["tags"])] == ["a","b","c"]
    resp = client.get('/cms/:c/')
    assert resp.status_code == 200
    assert list(resp.context['pages']) == pages[1:]
    assert [item["name"] for item in list(resp.context["tags"])] == ["a","b","c"]



def test_index_empty(db, client):
    resp = client.get('/cms/')
    assert resp.status_code == 200

    assert len(resp.context['pages']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No pages' in content


def test_show(pages, client):
    resp = client.get('/cms/abc/')
    assert resp.status_code == 200
    assert resp.context['page'] == pages[0]
    content = resp.content.decode(resp.charset)
    assert 'lorem ipsum' in content


def test_show_redirect(pages, client):
    resp = client.get('/cms/abc')
    assert resp.status_code == 301
    assert resp.url == '/cms/abc/'


def test_add(db, client):
    resp = client.post('/cms/+', {'title': 'Foo', 'content': 'Baaaar', 'tags': 'a,b,c'})
    content = resp.content.decode(resp.charset)
    print(content)
    assert resp.status_code == 302
    p = Page.objects.get()
    assert resp.url == '/cms/{}/'.format(p.slug)
    resp = client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Foo' in content
    assert 'Baaaar' in content
    assert 'href="/cms/:a/"' in content
    assert 'href="/cms/:b/"' in content


@pytest.mark.parametrize('data', [
    {'title': '', 'content': 'lorem ipsum'},
    {'title': 'foo', '': 'lorem ipsum'},
    {'title': 'foo', 'content': ''},
])
def test_add_empty(db, client, data):
    resp = client.post('/cms/+', data)
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content


def test_edit(pages, client):
    resp = client.get('/cms/foo/edit')
    assert resp.status_code == 200
    data = resp.context['form'].initial
    data['title'] = 'New Title'

    resp = client.post('/cms/foo/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/cms/foo/'

    resp = client.post(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'New Title' in content


def test_pageadmin(pages, client, admin_user):
    client.force_login(admin_user)
    resp = client.get('/admin/cmsapp/page/')
    assert resp.status_code == 200


def test_delete(pages, client):
    resp = client.post("/cms/foo/delete")
    assert resp.status_code == 302
    assert resp.url == "/cms/"
    assert list(Page.objects.all()) == pages[:2]
    resp = client.get("/cms/abc/delete")
    assert resp.status_code == 302
    assert resp.url == "/cms/abc/"
    assert list(Page.objects.all()) == pages[:2]
