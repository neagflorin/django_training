import pytest
from todoapp.models import ToDo, ListToDo
from test_cms import client


@pytest.fixture
def lists(request, db):
    l1 = ListToDo.objects.create(id=1, name="first")
    l2 = ListToDo.objects.create(id=2, name="second")
    lists = [l1, l2]
    return lists


@pytest.fixture
def todos(request, db):
    l1 = ListToDo.objects.get(name="first")
    l2 = ListToDo.objects.get(name="second")
    todos = [
        ToDo.objects.create(description="todo1", list=l1),
        ToDo.objects.create(description="todo2", list=l1),
        ToDo.objects.create(description="todo3", list=l2)
    ]
    return todos


def test_index(lists, client):
    response = client.get("/todo/")
    assert response.status_code == 200
    assert list(response.context["lists"]) == lists
    content = response.content.decode(response.charset)
    for _list in lists:
        assert _list.name in content


def test_index_empty(db, client):
    response = client.get("/todo/")
    assert response.status_code == 200
    assert len(response.context["lists"]) == 0
    content = response.content.decode(response.charset)
    assert "I don't have any lists!" in content



def test_todos(lists, todos, client):
    resp = client.get('/todo/1/')
    assert resp.status_code == 200
    assert list(resp.context['todos']) == todos[:2]
    assert resp.context['list'] == lists[0]
    content = resp.content.decode(resp.charset)
    assert 'todo1' in content
    assert 'todo2' in content


def test_add(lists, db, client):
    resp = client.post('/todo/2/add', {'description': 'add'})
    assert resp.status_code == 302
    assert resp.url == '/todo/2/'
    resp = client.get('/todo/2/add')
    assert resp.status_code == 200


def test_add_empty(lists, db, client):
    resp = client.post('/todo/1/add', {'description': ''})
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content


# def test_delete(lists, todos, client):
#     resp = client.post("/todo/2/delete")
#     assert resp.status_code == 302
#     assert resp.url == "/todo/2/"
#     assert list(ToDo.objects.all()) == todos[:2]

