import pytest


def test_simple():
    a, b = 1, 2
    assert a + b == 3
    assert a - b == -1


@pytest.fixture
def myfixture(request):
    yield [1, 2, 3]


def test_fixture(myfixture):
    assert myfixture == [1, 2, 3]


@pytest.mark.parametrize(['a', 'b'], [(1, 2),
                                      (2, 1),
                                      (0, 3)])
def test_param(a, b):
    assert a + b == 3