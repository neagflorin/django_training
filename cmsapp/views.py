# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import Page, Tag
from .forms import PageForm


def index(request, tag=None):
    if tag is None:
        pages = Page.objects.all()
    else:
        pages = Page.objects.filter(tag__name=tag)
    return render(request, "cmsapp/index.html", {
        'pages': pages,
        'tags': Tag.objects.values('name').distinct(),
    })


def page(request, slug):
    page = get_object_or_404(Page, slug=slug)
    return render(request, "cmsapp/page.html",
                  {'page':page,
                   'tags':page.tags.all()})


def change(request, slug=None):
    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(Page, slug=slug)
    if request.method == "POST":
        form = PageForm(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            return redirect("cmsapp:page", slug=instance.slug)
    else:
        form = PageForm(instance=instance)
    return render(request, "cmsapp/change.html", {
        'form': form, 'page': instance})

@login_required
def delete(request, slug):
    if request.method == "POST":
        instance = get_object_or_404(Page, slug=slug)
        instance.delete()
        return redirect("cmsapp:index")
    else:
        return redirect("cmsapp:page", slug=slug)