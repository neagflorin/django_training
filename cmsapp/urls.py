from django.conf.urls import url

from . import views

app_name = 'cmsapp'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^:(?P<tag>[^/]+)/$', views.index, name='index'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', views.page, name='page'),
    url(r'^\+$', views.change, name='change'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/edit$', views.change, name='change'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/delete$', views.delete, name='delete'),
]

