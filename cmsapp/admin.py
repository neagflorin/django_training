# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Page

# Register your models here.

# admin.site.register(Page) OR

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = 'pk', 'title', 'trimmed_content', 'slug'
    list_editable = ['title', 'slug']

    def trimmed_content(self, obj):
        return obj.content[:10]
    trimmed_content.admin_order_field = 'content'