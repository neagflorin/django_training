from django import forms

from .models import Page, Tag


class TagField(forms.CharField):
    def clean(self, value):
        value = super(TagField, self).clean(value)
        tags = [item.strip() for item in value.split(",")]
        return [tag for tag in tags if tag]


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = "title", "content"

    tags = TagField(required=False)

    def __init__(self, data=None, initial=None, instance=None, **kwargs):
        if instance:
            initial = initial or {}
            initial['tags'] = ', '.join(instance.tags.values_list('name',flat=True))
        return super(PageForm, self).__init__(data, initial=initial, instance=instance,
                                              **kwargs)

    def save(self, commit=True): # pragma: no cover
        instance = super(PageForm, self).save(commit)
        if commit:
            self.instance.tags.all().delete()
            Tag.objects.bulk_create([
                Tag(name=tag, page=self.instance)
                for tag in self.cleaned_data['tags']])
        return instance

