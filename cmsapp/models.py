# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from binascii import hexlify
from os import urandom


def make_default_slug():
    return hexlify(urandom(16))


# Create your models here.

class Page(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    slug = models.SlugField(max_length=200, unique=True, default=make_default_slug)

    def __repr__(self):  # https://pyformat.info/
        return "<Page #{0.pk} slug={0.slug!r} title={0.title!r}" \
               " content={0.content!r:.5}>".format(self)  # pragma: no cover


class Tag(models.Model):
    name = models.CharField(max_length=200)
    page = models.ForeignKey("Page", on_delete=models.CASCADE,related_name='tags',
                             related_query_name='tag')
