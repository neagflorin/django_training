from django.conf.urls import url

from . import views

app_name = 'todoapp'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<list_id>[0-9]+)/add$', views.add, name='add'),
    url(r'^(?P<list_id>[0-9]+)/$', views.todos, name='todos'),
    url(r'^(?P<list_id>[0-9]+)/delete$', views.delete, name='delete'),

]