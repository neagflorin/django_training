# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class ListToDo(models.Model):
    name = models.CharField(max_length=200)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name + " " + str(self.date.strftime("%Y-%m-%d %H:%M:%S")) # pragma: no cover


class ToDo(models.Model):
    description = models.CharField(max_length=200)
    list = models.ForeignKey("ListToDo", on_delete=models.CASCADE)

    def __str__(self):
        return '"{}"'.format(self.description) # pragma: no cover
