# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from todoapp.models import ListToDo, ToDo

admin.site.register(ListToDo)
admin.site.register(ToDo)