# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError

from .models import ListToDo, ToDo
from .forms import ToDoForm

# Create your views here.

def index(request):
    lists = ListToDo.objects.all()
    return render(request, "todoapp/index.html", {"lists": lists})


def todos(request, list_id):
    list = get_object_or_404(ListToDo, pk=list_id)
    todos = ToDo.objects.filter(list=list)
    return render(request, "todoapp/todos.html", {"list": list, "todos": todos})


def add(request, list_id):
    list = get_object_or_404(ListToDo, pk=list_id)
    if request.method == 'POST':
        form = ToDoForm(request.POST)
        if form.is_valid():
            ToDo.objects.create(description=form.cleaned_data["description"], list=list)
            return redirect("todoapp:todos", list_id=list.id)
    else:
        form = ToDoForm()
    return render(request, "todoapp/add.html", {'form': form, 'list': list})


def delete(request, list_id):
    if request.method == "POST":
        try:
            todo = get_object_or_404(ToDo, pk=request.POST["todo_id"])
            todo.delete()
        except MultiValueDictKeyError:
            print("select a todo")
        finally:
            return redirect("todoapp:todos", list_id=list_id)
    else:
        return redirect("todoapp:index")